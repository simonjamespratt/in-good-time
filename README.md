# In Good Time
In Good Time is a planning management application which can be used to:

* Plan and track the progress of projects
* Plan the duration and timing of tasks within a project
* Plan the duration and timing of multiple projects
* Assign tasks to team members and project their workload

Data is stored in a REST API built with Node.js, Express.js, Mongoose.js and MongoDB.

The API can be accessed using a custom user interface which has been built using Reactjs.

All terminal commands below should be run from the __ROOT__ folder of the project.

## Installation and Setup
This installation guide assumes you have Node, NPM and Mongo installed locally.

### Repository and Node Modules
Clone the repository from: [https://simonjamespratt@bitbucket.org/simonjamespratt/in-good-time.git](https://simonjamespratt@bitbucket.org/simonjamespratt/in-good-time.git)

The repository includes both the REST API and the UI

In the root of the project there is a package.json file which has a lot of important information for getting started.

In terminal, whilst in the root folder of the project (where the package.json file is), run: `npm install`

This will install all Node module dependencies including the dev dependencies. For a production installation, run: `npm install --production`.

### Database
The REST API saves to a Mongo database. Create a new Mongo database named "ingoodtime".


## Starting the REST API server
From the root of the project, run: `npm run start-rest`

If you wish to run the server using Nodemon (monitors for changes to your application's code and restarts the server if there is a change), instead of the above, run: `nodemon` (the "main" script in package.json file is set to the same command as is within "start-rest").

Note: When running this application, don't forget to start the Mongo daemon in terminal: `mongod`

If all is well, you should get a message in terminal which confirms that the server is running on port 3000 and that the database connection was successful. The API should now be accessible at localhost:3000.

## Starting the Webpack Dev Server (React UI)
For working locally on the React UI, the project can be run through a webpack dev server which allows hot loading and the ability to use the Chrome React inspector tool.

To run the webpack dev server, again in the root of the project, run: `start-webpack`

You should now be able to view the UI at: localhost:8080

## Builds and Production

Note that there are two output directories within the UI section of the project. The build folder is for development whilst the public folder is for production.

### Webpack - differences between development and production
When in development, the webpack-dev-server will hold the compiled scripts in memory and the effective contents of bundle.js will be served on the localhost:8080 server, along with the necessary static files from the "build" folder. You can also run `npm run webpack-build` which will build and persist the bundle.js file to the build directory.

For production, the bundle.js file will be served as a static file (instead of in memory). It and other static files will be served from the "public" directory.

The bundle.js file needs to be pre-compiled locally. This is done by running the script `npm run webpack-build-prod`.

The production build includes some performance-related improvements over the standard webpack build and due to their differences, they use differing webpack confguration files. See the root of the project for details.

### Gulp builds for styles
Finally, this project currently* compliles Sass to CSS using a gulp build.

You can build with: `gulp build`
You can watch with: `gulp watch`

The gulpfile is configured to build to both the "build" and "public" directories within the same task. See the gulpfile located in the root of the project for more details.

*This may be brought into the webpack build in future

