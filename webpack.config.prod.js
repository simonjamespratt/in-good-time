const webpack = require('webpack');
var path = require('path');

console.log("Prod config being used!");

module.exports = function(env) {
	return {
		entry: './ui/src/index.js',
		output: {
			path: __dirname + '/ui/public/',
			filename: 'bundle.js'
		},
		module: {
			loaders: [
				{
					test: /\.jsx?$/,
					exclude: /node_modules/,
					loader: "babel-loader"
				},
				{
					test: /\.css$/,
					loader: "style-loader!css-loader"
				}
			]
		},
		devtool: 'cheap-module-source-map',
		plugins: [
			new webpack.optimize.UglifyJsPlugin({
				sourceMap: true
			}),
			new webpack.DefinePlugin({
				'process.env.NODE_ENV': JSON.stringify('production'),
				API_URL: JSON.stringify('https://in-good-time.herokuapp.com/api') // this is from heroku
			})
		]
	}
}