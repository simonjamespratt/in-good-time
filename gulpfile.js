var gulp = require('gulp');
var sass = require('gulp-sass');
var maps = require('gulp-sourcemaps');
var prefix = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');



gulp.task('compileSass', function() {
	return gulp.src('./ui/src/styles/main.scss')
		.pipe(plumber(function(error) {
			console.log(error.message);
			this.emit('end');
		}))
		.pipe(maps.init())
		.pipe(sass({
			strictMath: true,
			strictUnits: true
		}))
		.pipe(prefix({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('./ui/build'))
		.pipe(cleanCSS({compatibility: '*' }))
		.pipe(rename('main.min.css'))
		.pipe(maps.write('./'))
		.pipe(gulp.dest('./ui/build'))
		.pipe(gulp.dest('./ui/public'));

});

gulp.task('build', ['compileSass']);

gulp.task('watch', function() {
  gulp.watch('./ui/src/styles/**/*.scss', ['compileSass']);
});

gulp.task('default', function() {
	console.log("Hello world! I am gulp!");
});