const webpack = require('webpack');

console.log("Dev config being used!");

module.exports = function(env) {
	return {
		entry: './ui/src/index.js',
		output: {
			path: __dirname + '/ui/build',
			publicPath: "/",
			filename: 'bundle.js'
		},
		module: {
			loaders: [
				{
					test: /\.jsx?$/,
					exclude: /node_modules/,
					loaders: ['react-hot-loader', 'babel-loader']
				},
				{
					test: /\.css$/,
					loader: "style-loader!css-loader"
				}
			]
		},
		devtool: 'cheap-module-source-map',
		plugins: [
			new webpack.DefinePlugin({
				API_URL: JSON.stringify('http://localhost:3000/api')
			})
		]
	}
}