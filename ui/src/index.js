import React from 'react';
import ReactDOM from 'react-dom';

import routes from './router.js';


ReactDOM.render(routes, document.getElementById('application'));