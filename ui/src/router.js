import React from 'react';
import ReactDOM from 'react-dom';

import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

// Component imports
import Header from './components/header.jsx';
import Home from './components/home.jsx';
import Team from './components/team.jsx';
import Projects from './components/projects.jsx';
import Project from './components/project.jsx';

// Routes
const routes = (
	<Router>
		<div className="inner">

			<Header/>

			<Route exact path="/" component={Home} />
			<Route path="/team" component={Team} />
			<Route exact path="/projects" component={Projects} />
			<Route path="/projects/:id" component={Project} />
			
		</div>
	</Router>
);

export default routes;

