import React from 'react';

var moment = require('moment');

function ProjectOverview(props) {
	return (
		<div>
			<h2 className="document__title">{props.projectData.name}</h2>
			
			<dl className="document__item document__item--short">
				<dt>Project starts on</dt>
				<dd>{moment(props.projectData.start_date).format(props.dateFormat)}</dd>
			</dl>
			<dl className="document__item document__item--short">
				<dt>Project ends on</dt>
				<dd>{moment(props.projectData.end_date).format(props.dateFormat)}</dd>
			</dl>

			<button className="button--neutral--inline" onClick={props.updateProject}>Update overview</button>
		</div>
	);
}

export default ProjectOverview;
