import React from 'react';

import ProjectForm from './project-form.jsx';
import ProjectOverview from './project-overview.jsx';
import TaskListing from './task-listing.jsx';
import TaskDetails from './task-details.jsx';
import TaskForm from './task-form.jsx';

var moment = require('moment');


// NOTE ON DATES:
// Keep dates in the PROJECT DATA array and when sending to SERVER as STRINGS.
// Keep dates in the FORM VALUES objects as MOMENTS
class Project extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			projectData: [],
			addProject: false,
			showForm: false,
			formValues: {
				name: "",
				startDate:  moment(),
				endDate: moment()			
			},
			isUpdate: false,
			dateFormatLong: "Do MMMM YYYY",
			dateFormatShort: "DD MMM",
			showTaskDetails: false,
			activeTask: null,
			showTaskForm: false,
			teamMembers: null,
			taskFormValues: {
				name: "",
				startDate: moment(),
				endDate: moment(),
				description: "",
				assignee: "unassigned"
			},
			isTaskUpdate: false
		}

		this.toggleFormState = this.toggleFormState.bind(this);
		this.toggleTaskFormState = this.toggleTaskFormState.bind(this);
		this.handleFormInputChange = this.handleFormInputChange.bind(this);
		this.handleStartDateChange = this.handleStartDateChange.bind(this);
		this.handleEndDateChange = this.handleEndDateChange.bind(this);
		this.handleFormSubmit = this.handleFormSubmit.bind(this);
		this.updateProject = this.updateProject.bind(this);
		this.viewTaskDetails = this.viewTaskDetails.bind(this);
		this.addTask = this.addTask.bind(this);
		this.handleTaskFormInputChange = this.handleTaskFormInputChange.bind(this);
		this.handleTaskStartDateChange = this.handleTaskStartDateChange.bind(this);
		this.handleTaskEndDateChange = this.handleTaskEndDateChange.bind(this);
		this.handleTaskFormSubmit = this.handleTaskFormSubmit.bind(this);
		this.updateTask = this.updateTask.bind(this);
		this.getTeamMembers = this.getTeamMembers.bind(this);
		this.deleteTask = this.deleteTask.bind(this);
		this.taskStatus = this.taskStatus.bind(this);
		this.clearTaskFormValues = this.clearTaskFormValues.bind(this);
		this.cancelTaskForm = this.cancelTaskForm.bind(this);
	}

	componentDidMount() {
		if (this.props.match.params.id === 'new-project') {
			this.setState({addProject: true});
			this.setState({showForm: true});
		} else {
			fetch(`${API_URL}/projects/${this.props.match.params.id}`)
				.then(response => response.json())
				.then(responseData => {
					this.setState({ projectData: responseData });
				})
				.catch(error => {
					console.log("Error fetching and parsing data", error);
				});	
		}
	}

	/*===============================
	=            Project            =
	===============================*/	

	toggleFormState() {
		this.setState(prevState => ({
			showForm: !prevState.showForm
		}));
	}

	handleStartDateChange(moment) {
		this.handleFormInputChange(moment, "startDate");
	}

	handleEndDateChange(moment) {
		this.handleFormInputChange(moment, "endDate");
	}

	handleFormInputChange(event, date = false) {
		var values = this.state.formValues;
		if(date) {			
			values[date] = event;
		} else {
			values[event.target.name] = event.target.value;
		}
		this.setState({
			formValues: values
		});
	}

	handleFormSubmit(event) {
		var reqBody = {
			name: this.state.formValues.name,
			start_date: this.state.formValues.startDate.toISOString(),
			end_date: this.state.formValues.endDate.toISOString()
		}

		if(this.state.isUpdate) {
			fetch(`${API_URL}/projects/${this.state.projectData._id}`,
			{
	    		headers: {
	      			'Accept': 'application/json',
	      			'Content-Type': 'application/json'
	    		},
	    		method: "PUT",
	   			body: JSON.stringify(reqBody)
			})
			.then(response => response.json())
			.then(responseData => {
				this.setState({projectData: responseData});
			})
			.catch(error => { console.log("Error whilst attempting to update new team member", error) })

			// Reset update state
			this.setState({
				isUpdate: false
			});
			
		} else {
			fetch(`${API_URL}/projects`,
			{
	    		headers: {
	      			'Accept': 'application/json',
	      			'Content-Type': 'application/json'
	    		},
	    		method: "POST",
	   			body: JSON.stringify(reqBody)
			})
			.then(response => response.json())
			.then(responseData => {
					console.log(responseData);
					this.setState({ projectData: responseData });
					this.props.history.push(`/projects/${responseData._id}`);
			})
			.catch(error => { console.log("Error whilst attempting to create new team member", error) });
		}

		event.preventDefault();

		//reset values to empty
		this.setState({
			formValues: {
				name: "",
				startDate: moment(),
				endDate: moment()
			}
		});
		this.toggleFormState();
	}

	updateProject() {
		this.setState({
			isUpdate: true,
			formValues: {
				name: this.state.projectData.name,
				startDate: moment(this.state.projectData.start_date),
				endDate: moment(this.state.projectData.end_date)
			}
		});

		this.toggleFormState();
	}

	/*============================
	=            Task            =
	============================*/	

	toggleTaskFormState() {
		this.setState(prevState => ({
			showTaskForm: !prevState.showTaskForm
		}));
	}

	handleTaskStartDateChange(moment) {
			this.handleTaskFormInputChange(moment, "startDate");
	}

	handleTaskEndDateChange(moment) {
		this.handleTaskFormInputChange(moment, "endDate");
	}

	handleTaskFormInputChange(event, date = false) {
		var values = this.state.taskFormValues;
		if(date) {
			values[date] = event;
		} else {
			values[event.target.name] = event.target.value;
		}
		this.setState({
			taskFormValues: values
		});
	}

	clearTaskFormValues() {
		// reset form values and display logic
		this.setState({
			taskFormValues: {
				name: "",
				startDate: moment(),
				endDate: moment(),
				description: "",
				assignee: "unassigned"
			},
		});
	}

	handleTaskFormSubmit(event) {
		// Note: REST API can only persist a valid ObjectID for the field of assignee.
		// Thus if user leaves it unassigned, we need to avoid sending a value for that field at all.
		let assignee = this.state.taskFormValues.assignee;
		var reqBody = {
			name: this.state.taskFormValues.name,
			start_date: this.state.taskFormValues.startDate.toISOString(),
			end_date: this.state.taskFormValues.endDate.toISOString(),
			description: this.state.taskFormValues.description,
			assignee: assignee
		}

		if (assignee == 'unassigned') {
			delete reqBody.assignee;
		}

		if(this.state.isTaskUpdate) {
			fetch(`${API_URL}/projects/${this.state.projectData._id}/tasks/${this.state.activeTask._id}`,
			{
	    		headers: {
	      			'Accept': 'application/json',
	      			'Content-Type': 'application/json'
	    		},
	    		method: "PUT",
	   			body: JSON.stringify(reqBody)
			})
			.then(response => response.json())
			.then(responseData => {
				console.log(responseData);
				this.setState({projectData: responseData});
				this.viewTaskDetails(this.state.activeTask._id);
			})
			.catch(error => { console.log("Error whilst attempting to update task", error) })


			// Reset update state
			this.setState({
				isTaskUpdate: false,
				showTaskDetails: true
			});

		} else {

			fetch(`${API_URL}/projects/${this.state.projectData._id}/tasks`,
				{
		    		headers: {
		      			'Accept': 'application/json',
		      			'Content-Type': 'application/json'
		    		},
		    		method: "POST",
		   			body: JSON.stringify(reqBody)
				})
				.then(response => response.json())
				.then(responseData => {
						this.setState({ projectData: responseData });
				})
				.catch(error => { console.log("Error whilst attempting to create new task", error) })
		}

		this.clearTaskFormValues();
		this.setState({showTaskForm: false});

		event.preventDefault();

	}

	viewTaskDetails(task_id) {
		var activeTask = this.state.projectData.tasks.find(obj => obj._id === task_id);

		this.setState({
			showTaskDetails: true,
			activeTask: activeTask,
			showTaskForm: false
		});
	}

	getTeamMembers() {
		if (this.state.teamMembers === null) {
			fetch(`${API_URL}/team`)
				.then(response => response.json())
				.then(responseData => {
					this.setState({ teamMembers: responseData });
				})
				.catch(error => {
					console.log("Error fetching and parsing data for teamMembers", error);
				});	
		}
	}

	addTask(event) {
		this.getTeamMembers();

		this.setState({
			showTaskDetails: false,
			showTaskForm: true
		});

		event.preventDefault();
	}

	updateTask() {
		this.getTeamMembers();

		this.setState({
			isTaskUpdate: true,
			taskFormValues: {
				name: this.state.activeTask.name,
				startDate: moment(this.state.activeTask.start_date),
				endDate: moment(this.state.activeTask.end_date),
				description: this.state.activeTask.description,
				assignee: this.state.activeTask.assignee
			},
			showTaskDetails: false,
			showTaskForm: true
		});

		event.preventDefault();
	}

	cancelTaskForm() {
		this.clearTaskFormValues();
		this.toggleTaskFormState();
	}

	deleteTask(task_id) {
		fetch(`${API_URL}/projects/${this.state.projectData._id}/tasks/${task_id}`,
		{
    		method: "DELETE"
		})
		.then(response => {
			if(response.status === 200) {
				return response.json();
			}
		})
		.then(responseData => {
			this.setState({ projectData: responseData });
		})
		.catch(error => { console.log("There was an error whilst attempting to delete a task", error) })
		event.preventDefault();

		if(this.state.activeTask._id === task_id) {
			this.setState({
				activeTask: null,
				showTaskDetails: false
			});

		}
	}

	taskStatus(task_id, task_status) {
		var status;
		if (task_status) {
			status = "incomplete";
		} else {
			status = "complete";
		}

		fetch(`${API_URL}/projects/${this.state.projectData._id}/tasks/${task_id}/${status}`,
		{
    		method: "POST"
		})
		.then(response => response.json())
		.then(responseData => {
			this.setState({ projectData: responseData });

			if(this.state.showTaskDetails) {
				this.viewTaskDetails(task_id);
			}
		})
		.catch(error => { console.log("There was an error whilst attempting to change the status of a task", error) })
		event.preventDefault();
	}

	/*==============================
	=            Render            =
	==============================*/	

	render() {
		let project;
		let tasks = null;
		let taskDetails = null;
		let taskForm = null;
		if (this.state.showForm === true) {
			project = <ProjectForm
						handleChange={this.handleFormInputChange}
						handleSubmit={this.handleFormSubmit}
						formValues={this.state.formValues}
						isUpdate={this.state.isUpdate}
						handleStartDateChange={this.handleStartDateChange}
						handleEndDateChange={this.handleEndDateChange} />;
		} else {
			project =	<div>
							<ProjectOverview 
								projectData={this.state.projectData}
								dateFormat={this.state.dateFormatLong}
								updateProject={this.updateProject.bind(this)} />
						</div>;
		}

		if (this.state.projectData.tasks) {
			tasks = 
				<div>
					<h3 className="document__sub-title">Tasks</h3>
					<ul className="list tasks">
						{this.state.projectData.tasks.map(
							function(task, index) {
								return <TaskListing 
											key={task._id} task={task} 
											dateFormat={this.state.dateFormatShort} 
											viewTaskDetails={this.viewTaskDetails}
											deleteTask={this.deleteTask}
											taskStatus={this.taskStatus} />
							}.bind(this)
						)}
					</ul>
					{this.state.showTaskForm === false &&
						<button className="button--positive--removed" onClick={this.addTask}>Add task</button>
					}
				</div>
		} else {
			tasks = null;
		}

		if (this.state.showTaskDetails === true) {
			taskDetails = <TaskDetails 
									task={this.state.activeTask} 
									dateFormat={this.state.dateFormatLong} 
									update={this.updateTask} />
		} else {
			taskDetails = null;
		}

		if (this.state.showTaskForm === true) {
			taskForm = <TaskForm 
							handleChange={this.handleTaskFormInputChange} 
							handleSubmit={this.handleTaskFormSubmit}
							team={this.state.teamMembers}
							formValues={this.state.taskFormValues}
							cancelTaskForm={this.cancelTaskForm}
							handleTaskStartDateChange={this.handleTaskStartDateChange}
							handleTaskEndDateChange={this.handleTaskEndDateChange} />
		} else {
			taskForm = null;
		}

		return(
			<div className="container project">
				<div className="layout layout--half">
					<div className="document project">
						{project}
						{tasks}
					</div>
				</div>
				{taskDetails}
				{taskForm}
			</div>
		);
	}
}

export default Project;