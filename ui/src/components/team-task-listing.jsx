import React from 'react';

import { Link } from 'react-router-dom';

var moment = require('moment');

function TeamTaskListing(props) {
	return(
		<div className="document team-member-tasks">
			<h2 className="document__title">{props.memberName}&prime;s tasks</h2>
			{props.projects.map(function(project, index) {
				return(
					<div key={project._id}>
						<h3 className="document__sub-title">{project.name}</h3>
						<Link className="button--neutral--inline" to={`/projects/${project._id}`}>View project</Link>
						<ul className="list">
						{project.tasks.map(function(task, index) {
							return(
								<li className="list__item" key={task._id}>
									<span className="list__title list__title--member-task">{task.name}</span>
									<span className="list__date list__date--member-task">{moment(task.start_date).format(props.dateFormatShort)}</span>
									<span className="list__date list__date--member-task list__date--end">{moment(task.end_date).format(props.dateFormatShort)}</span>
									<span className={task.complete ? "list__status list__status--complete" : "list__status"}>
										<span>Status</span>
										<span className="indicator"> {task.complete ? "complete" : "incomplete"}</span>
									</span>
								</li>
							);
						})}
						</ul>
					</div>	
				);
			})}
		</div>
	);
}

export default TeamTaskListing;
