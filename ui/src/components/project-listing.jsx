import React from 'react';

import { Link } from 'react-router-dom';

var moment = require('moment');

function ProjectListing(props) {
	var deleteProject = () => props.deleteProject(props.id);
	return(
		<li className="list__item list__item--projects project-listing">
			<span className="list__title list__title--projects">{props.name}</span>
			<span className="list__date list__date--projects">{moment(props.startDate).format(props.dateFormat)}</span>
			<span className="list__date-separator">to</span>
			<span className="list__date list__date--projects">{moment(props.endDate).format(props.dateFormat)}</span>
			<Link className="button--neutral--inline button--icon--icon-replace--view" to={`/projects/${props.id}`}>View</Link>
			<button className="button--negative--inline button--icon--icon-replace--delete" onClick={deleteProject}>Delete</button>
		</li>
	);
}

export default ProjectListing;