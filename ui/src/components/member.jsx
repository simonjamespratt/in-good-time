import React from 'react';

function Member(props) {
	var deleteMember = () => props.deleteMember(props.id);
	var updateMember = () => props.updateMember(props.id, props.name);
	var viewTasks = () => props.viewTasks(props.id, props.name);

	return(
		<li className="list__item member">
			<span className="list__title list__title--team-member">{props.name}</span>
			<button className="button--icon--view" onClick={viewTasks}>View tasks</button>
			<button className="button--icon--edit" onClick={updateMember}>Edit</button>
			<button className="button--icon--delete" onClick={deleteMember}>Delete</button>
		</li>
	);
}

export default Member;