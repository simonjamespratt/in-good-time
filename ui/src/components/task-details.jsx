import React from 'react';
import { Link } from 'react-router-dom';

var moment = require('moment');

function TaskDetails(props) {
	var status = null;
	if (props.task.complete === true) {
		status = 'complete';
	} else {
		status = 'incomplete';
	}
	return(
		<div className="layout layout--half layout--half--last">
			<div className="document task-details">
				<h2 className="document__title">{props.task.name}</h2>
				
				<button className="button--neutral--inline button--spaced" onClick={props.update}>Update task</button>
				
				<dl className="document__item document__item--short">
					<dt>Start date</dt>
					<dd>{moment(props.task.start_date).format(props.dateFormat)}</dd>
				</dl>

				<dl className="document__item document__item--short">
					<dt>End date</dt>
					<dd>{moment(props.task.end_date).format(props.dateFormat)}</dd>
				</dl>

				<dl className="document__item document__item--short">
					<dt>Status</dt>
					<dd>{status}</dd>
				</dl>

				<dl className="document__item document__item--long">
					<dt>Description</dt>
					<dd>{props.task.description}</dd>
				</dl>

				<dl className="document__item document__item--short">
					<dt>Assigned to</dt>
					<dd>{props.task.assignee ? props.task.assignee.name : "unassigned"}</dd>
				</dl>

			</div>
		</div>
	);
}

export default TaskDetails;