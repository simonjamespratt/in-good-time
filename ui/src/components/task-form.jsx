import React from 'react';

var DatePicker = require('react-datepicker');
var moment = require('moment');

require('react-datepicker/dist/react-datepicker.css');

function TaskForm(props) {
	return(
 		<div className="layout layout--half layout--half--last">
 			<div className="document task-form">
			 	<h2 className="document__title">New task</h2>
				<form className="form" onSubmit={props.handleSubmit}>
					<label className="form__label">
						Task:
						<input className="form__input" name="name" type="text" value={props.formValues.name} onChange={props.handleChange} />
					</label>
					<label className="form__label">
						Start date:
						<DatePicker name="startDate" className="form__input form__input--date" dateFormat="DD/MM/YYYY" selected={props.formValues.startDate} onChange={props.handleTaskStartDateChange} todayButton={"Today"} placeholderText="Click to select a date" />
					</label>
					<label className="form__label">
						End date:
						<DatePicker name="endDate" className="form__input form__input--date" dateFormat="DD/MM/YYYY" selected={props.formValues.endDate} onChange={props.handleTaskEndDateChange} todayButton={"Today"} placeholderText="Click to select a date" />
					</label>
					<label className="form__label">
						Description:
						<textarea className="form__textarea" name="description" value={props.formValues.description} onChange={props.handleChange} />
					</label>
					
					<label className="form__label form__label--last">
						Assignee:
						<select className="form__select" name="assignee" value={props.formValues.assignee} onChange={props.handleChange} >
						{props.team.map(function(member, index) {
							return <option key={member._id} value={member._id}>{member.name}</option>
						})}
									<option value="unassigned">Select an assignee</option>
						</select>
					</label>
					
					<input className="button--positive--removed" type="submit" value="submit" />
				</form>

				<button className="button--neutral" onClick={props.cancelTaskForm}>Cancel</button>
 			</div>
 		</div>
	);
 }

export default TaskForm;