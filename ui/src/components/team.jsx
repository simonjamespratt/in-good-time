import React from 'react';

import Member from './member.jsx';
import MemberForm from './member-form.jsx';
import TeamTaskListing from './team-task-listing.jsx';

class Team extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			members: [],
			showForm: false,
			isUpdate: false,
			updateID: null,
			formValues: "",
			memberTasks: [],
			activeMember: false,
			activeMemberName: null,
			dateFormatLong: "Do MMMM YYYY",
			dateFormatShort: "DD MMM"
		};

		this.toggleFormState = this.toggleFormState.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.newMember = this.newMember.bind(this);
		this.updateMember = this.updateMember.bind(this);
		this.deleteMember = this.deleteMember.bind(this);
		this.viewTasks = this.viewTasks.bind(this);
	}

	componentDidMount() {
		// const memberId = this.props.match.params.id;
		
		fetch(`${API_URL}/team`)
			.then(response => response.json())
			.then(responseData => {
				this.setState({ members: responseData });
			})
			.then( () => {
				// NOTE: Re-instate this if can find a way to pass params through router without altering the URL.
				// if(memberId) {
				// 	let memberName = this.state.members.find(obj => obj._id === memberId);
				// 	memberName = memberName.name;
				// 	this.viewTasks(memberId, memberName);
				// }
			})
			.catch(error => {
				console.log("Error fetching and parsing data", error);
			});
			

	}
	
	handleChange(event) {
		this.setState({formValues: event.target.value})
	}

	handleSubmit(event) {

		var reqBody =  {
			name: this.state.formValues
		};

		if(this.state.isUpdate) {
			fetch(`${API_URL}/team/${this.state.updateID}`,
			{
	    		headers: {
	      			'Accept': 'application/json',
	      			'Content-Type': 'application/json'
	    		},
	    		method: "PUT",
	   			body: JSON.stringify(reqBody)
			})
			.then(response => response.json())
			.then(responseData => {
				var membersClone = this.state.members.slice();
				var index = membersClone.findIndex(obj => obj._id === responseData._id);
				if (index !== -1) {
   					membersClone[index] = responseData;
				}
				this.setState({members: membersClone});

			})
			.catch(error => { console.log("Error whilst attempting to update new team member", error) })

			// Reset update states
			this.setState({
				isUpdate: false,
				updateID: null
			});
			
		} else {
			fetch(`${API_URL}/team`,
			{
	    		headers: {
	      			'Accept': 'application/json',
	      			'Content-Type': 'application/json'
	    		},
	    		method: "POST",
	   			body: JSON.stringify(reqBody)
			})
			.then(response => response.json())
			.then(responseData => {
				var membersClone = this.state.members.slice();
				membersClone.push(responseData);
				this.setState({members: membersClone});

			})
			.catch(error => { console.log("Error whilst attempting to create new team member", error) })
		}

		event.preventDefault();
		this.setState({formValues: ""}); //reset values to empty
		this.toggleFormState();
	}

	toggleFormState() {
		this.setState(prevState => ({
			showForm: !prevState.showForm
		}));
	}

	newMember() {
		this.setState({
			activeMember: false,
			isUpdate: false,
			formValues: ""
		});
		this.toggleFormState();
	}

	deleteMember(member_id) {
		fetch(`${API_URL}/team/${member_id}`,
		{
    		method: "DELETE"
		})
		.then(response => {
			if(response.status === 200) {
				return response.json();
			}
		})
		.then(responseData => {
			this.setState({ members: responseData });
		})
		.catch(error => { console.log("There was an error whilst attempting to delete a team member", error) })
		event.preventDefault();

		this.setState({
			showForm: false,
			activeMember: false
		});
	}

	updateMember(member_id, member_name) {
		this.setState({
			isUpdate: true,
			updateID: member_id,
			formValues: member_name,
			activeMember: false,
			showForm: true
		});

	}

	viewTasks(member_id, member_name) {
		fetch(`${API_URL}/projects/member-tasks/${member_id}`)
			.then(response => response.json())
			.then(responseData => {
				this.setState({ memberTasks: responseData });
			})
			.catch(error => {
				console.log("Error fetching and parsing data", error);
			});	

		this.setState({
			activeMember: true,
			activeMemberName: member_name,
			showForm: false
		});
	}

	render() {
		const renderForm = this.state.showForm;
		let memberForm = null;
		let addMemberButton = null;
		let memberTasksView = null;

		if (renderForm) {
			memberForm = <MemberForm 
				handleChange={this.handleChange}
				handleSubmit={this.handleSubmit}
				formValues={this.state.formValues}
				toggleForm={this.toggleFormState}
				isUpdate={this.state.isUpdate} />;

			addMemberButton = null;

		} else {
			addMemberButton = <button className="button--positive--removed" onClick={this.newMember}>Add member</button>;
			memberForm = null;
		}

		if(this.state.activeMember) {
			memberTasksView = <TeamTaskListing
									memberName={this.state.activeMemberName} 
									projects={this.state.memberTasks}
									dateFormatShort={this.state.dateFormatShort} />
		}

		return(
			<div className="container team">
				<div className="layout layout--half">
					<div className="document team-listing">
						<h2 className="document__title">Team members</h2>
						<ul className="list members">
							{this.state.members.map(function(member, index) {
								return <Member 
									name={member.name} 
									key={member._id} 
									id={member._id} 
									deleteMember={this.deleteMember}
									updateMember={this.updateMember}
									viewTasks={this.viewTasks} />
							}.bind(this))}
						</ul>
						{addMemberButton}
					</div>
				</div>

				<div className="layout layout--half layout--half--last">
					{memberForm}
					{memberTasksView}
				</div>
			</div>
		);
	}
}

export default Team;
