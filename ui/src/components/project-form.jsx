import React from 'react';

import { Link } from 'react-router-dom';
var DatePicker = require('react-datepicker');
var moment = require('moment');

require('react-datepicker/dist/react-datepicker.css');

function ProjectForm(props) {
	return (
		<div>
			<h2 className="document__title">{props.isUpdate ? "Update project" : "New project"}</h2>
			<form className="form" onSubmit={props.handleSubmit}>
				<label className="form__label">
					Project Name:
					<input className="form__input" name="name" type="text" value={props.formValues.name} onChange={props.handleChange} />
				</label>
				<label className="form__label">
					Start date:
					<DatePicker name="startDate" className="form__input form__input--date" dateFormat="DD/MM/YYYY" selected={props.formValues.startDate} onChange={props.handleStartDateChange} todayButton={"Today"} placeholderText="Click to select a date" />
				</label>
				<label className="form__label">
					End date:
					<DatePicker name="endDate" className="form__input form__input--date form__label--last" dateFormat="DD/MM/YYYY" selected={props.formValues.endDate} onChange={props.handleEndDateChange} todayButton={"Today"} placeholderText="Click to select a date" />
				</label>

				<input className="button--positive--removed" type="submit" value="submit" />
			</form>
			<Link className="button--neutral" to="/projects">Cancel</Link>
		</div>
	);
}

export default ProjectForm;