import React from 'react';

var moment = require('moment');

function TaskListing(props) {
	var viewTaskDetails = () => props.viewTaskDetails(props.task._id);
	var deleteTask = () => props.deleteTask(props.task._id);
	var taskStatus = () => props.taskStatus(props.task._id, props.task.complete);

	var buttonStatusText = "Not done",
		buttonStatusClass = "button--status",
		titleClass = "list__title list__title--tasks",
		dateStartClass = "list__date list__date--tasks",
		dateEndClass = "list__date list__date--tasks list__date--end list__date--tasks--end";

	if (props.task.complete) {
		buttonStatusText = "Done",
		buttonStatusClass = "button--status--complete",
		titleClass = "list__title list__title--tasks list__title--complete",
		dateStartClass = "list__date list__date--tasks list__date--complete",
		dateEndClass = "list__date list__date--tasks list__date--end list__date--tasks--end list__date--complete";
	}

	return(
		<li className="list__item">
			<button className={buttonStatusClass} onClick={taskStatus}>{buttonStatusText}</button>
			<span className={titleClass}>{props.task.name}</span>
			<span className={dateStartClass}>{moment(props.task.start_date).format(props.dateFormat)}</span>
			<span className={dateEndClass}>{moment(props.task.end_date).format(props.dateFormat)}</span>
			<button className="button--icon--view" onClick={viewTaskDetails}>View</button>
			<button className="button--icon--delete" onClick={deleteTask}>Delete</button>
		</li>
	);
}

export default TaskListing;