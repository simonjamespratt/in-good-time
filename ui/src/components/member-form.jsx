import React from 'react';

function MemberForm(props) {
	return (
		<div className="document member-form">
			<h2 className="document__title">{props.isUpdate ? "Update team member" : "New team member"}</h2>
			<form className="form" onSubmit={props.handleSubmit}>
				<label className="form__label form__label--last">
					Name:
					<input className="form__input" type="text" value={props.formValues} onChange={props.handleChange} />
				</label>
				<input className="button--positive--removed" type="submit" value="submit" />
			</form>
			<button className="button--neutral" onClick={props.toggleForm}>Cancel</button>
		</div>
	);
}

export default MemberForm;