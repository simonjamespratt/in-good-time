import React from 'react';

import {Link} from 'react-router-dom';

function Home() {
  return (
  	<div className="container home">
	  	<p>Hello and welcome. In Good Time is an application for managing projects and your team's time in order to get projects done as efficiently as possible.</p>
	  	<p>Why not start my creating your <Link to="/team">team members...</Link></p>
  	</div>
  );
}

export default Home;