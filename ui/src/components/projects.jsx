import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import ProjectListing from './project-listing.jsx';

class Projects extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			projects: []
		}

		this.deleteProject = this.deleteProject.bind(this);
	}

	componentDidMount() {
		fetch(`${API_URL}/projects`)
			.then(response => response.json())
			.then(responseData => {
				this.setState({ projects: responseData });
			})
			.catch(error => {
				console.log("Error fetching and parsing data", error);
			});	
	}

	deleteProject(project_id) {
		fetch(`${API_URL}/projects/${project_id}`,
		{
    		method: "DELETE"
		})
		.then(response => {
			if(response.status === 200) {
				return response.json();
			}
		})
		.then(responseData => {
			this.setState({ projects: responseData });
		})
		.catch(error => { console.log("There was an error whilst attempting to delete a project", error) })
		event.preventDefault();
	}

	render() {
		return(
			<div className="container projects">
				<div className="layout layout--full">
					<div className="document document--projects">
						<h2 className="document__title">Projects</h2>
						<ul className="list project-listings">
							{this.state.projects.map(function(project, index) {
								return <ProjectListing
											name={project.name}
											startDate={project.start_date}
											endDate={project.end_date}
											dateFormat={'Do MMM'}
											id={project._id}
											key={project._id}
											deleteProject={this.deleteProject.bind(this)} />
							}.bind(this))}
						</ul>
						<div><Link className="button--positive--removed" id="new-project" to="/projects/new-project">Add new project</Link></div>
					</div>
				</div>
			</div>
		);
	}
}

export default Projects;