import React from 'react';

import { NavLink } from 'react-router-dom';

function Header() {
	return (
			<header className="header">
				<h1 className="header__title"><NavLink exact to="/">In good time</NavLink></h1>
				<nav className="nav-outer">
					<ul className="nav">
						<li className="nav__item"><NavLink to="/team">Team</NavLink></li>
						<li className="nav__item"><NavLink to="/projects">Projects</NavLink></li>
					</ul>
				</nav>
			</header>
	);
}

export default Header;