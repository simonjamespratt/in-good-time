'use strict';

/*================================
=            REQUIRES            =
================================*/
var path = require('path');
var express = require('express');
var teamRoutes = require("./routes/team");
var projectsRoutes = require("./routes/projects");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var logger = require("morgan");

var app = express();

/*===========================================
=            DATABASE CONNECTION            =
===========================================*/

var dbpath;
if (process.env.NODE_ENV == 'production') {
	dbpath = process.env.MONGODB_URI;
} else {
	dbpath = "mongodb://localhost:27017/ingoodtime";
}

mongoose.connect(dbpath);

var db = mongoose.connection;

db.on("error", function(err){
	console.error("connection error:", err);
});

db.once("open", function(){
	console.log("db connection successful");
});

/*==================================
=            MIDDLEWARE            =
==================================*/

// A function to revive dates that are passed in the JSON request body as string values in the format ISO8601
// which need to be converted back to a Date object
// function reviveDates(key, value){
// var regexIso8601 = /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})\.(\d{1,})(Z|([\-+])(\d{2}):(\d{2}))?)?)?)?$/;
// var match;
// if (typeof value === "string" && (match = value.match(regexIso8601))) {
//   var milliseconds = Date.parse(match[0]);
//   if (!isNaN(milliseconds)) {
//     return new Date(milliseconds);
//   }
// }
// return value;
// }

app.use(function(req, res, next){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	if(req.method === "OPTIONS") {
		res.header("Access-Control-Allow-Methods", "PUT,POST,DELETE");
		return res.status(200).json({});
	}
	next();
});

app.use(bodyParser.json());

app.use(logger('dev'));

/*==============================
=            ROUTES            =
==============================*/

app.use("/api/team", teamRoutes);
app.use("/api/projects", projectsRoutes);

// Production environment only - allows access to the UI via "/" route
if(process.env.NODE_ENV == 'production') {
	app.use(express.static('./ui/public/'));
	app.use("/", function(req, res) {
		// res.sendFile(__dirname + '/ui/public/index.html');
		res.sendFile('./ui/public/index.html');
	});	
}


/*======================================
=            ERROR HANDLING            =
======================================*/
// catch 404 and forward to error handler
app.use(function(req, res, next){
	var err = new Error("Not Found");
	err.status = 404;
	next(err);
});

// Error Handler
app.use(function(err, req, res, next){
	res.status(err.status || 500);
	res.json({
		error: {
			message: err.message
		}
	});
});

/*==================================
=            RUN SERVER            =
==================================*/
var port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log('REST API for In Good Time running on port' + port);
});