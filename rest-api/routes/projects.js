'use strict';

var express = require('express');
var projectsRouter = express.Router();
var Project = require('../models/project_model.js');


/*==========================================
=            Param ID detection            =
==========================================*/

// These look for a specified parameter (e.g. "pID") in a request url.
// If found, they return the document pertaining to that ID within the specified collection.
projectsRouter.param("pID", function(req,res,next,id){
	Project
	.findById(id)
	.populate('tasks.assignee')
	.exec(function(err, doc){
		if(err) return next(err);
		if(!doc) {
			err = new Error("Not Found");
			err.status = 404;
			return next(err);
		}
		req.project = doc;
		return next();
	});
});

// This one returns the sub-document of the main document (task belonging to a project).
projectsRouter.param("taID", function(req,res,next,id){
	req.task = req.project.tasks.id(id);
	if(!req.task) {
		err = new Error("Not Found");
		err.status = 404;
		return next(err);
	}
	next();
});

/*=====  End of Param ID detection  ======*/

/*=============================================
=            Project object routes            =
=============================================*/

// GET /projects
// Route for viewing the projects collection
projectsRouter.get("/", function(req, res, next){
	Project.find(function(err, projects) {
		if(err) return next(err);
		res.json(projects);
	});
});

// GET /projects/member-tasks/:member_id
// Route for viewing the projects collection, filtered by team member ID
projectsRouter.get("/member-tasks/:member_id", function(req, res, next) {
	Project
	.find({"tasks.assignee": req.params.member_id})
	.select({name: 1, start_date: 1, end_date: 1, tasks:{$elemMatch:{assignee: req.params.member_id}}})
	.exec(function(err, projects) {
		if(err) return next(err);
		res.json(projects);
	});	
});

// POST /projects
// Route for creating a project
projectsRouter.post("/", function(req, res, next){
	var project = new Project(req.body);
	project.save(function(err, project) {
		if(err) return next(err);
		res.status(201);
		res.json(project);
	});
});

// GET /projects/:pID
// Route for viewing a single project
projectsRouter.get("/:pID", function(req, res, next){
	res.json(req.project); // all the heavy lifting for this is done above in the params ID detection section
});

// PUT /projects/:pID
// Route for updating a single project
projectsRouter.put("/:pID", function(req, res, next){
	Project.findByIdAndUpdate(req.params.pID, {$set: req.body}, {new: true}, function(err, updatedProject) {
		if(err) return next(err);
		res.json(updatedProject);
	});

	// req.project.update(req.body, function(err, result) {
	// 	if(err) return next(err);
	// 	res.json(result);
	// });
});

// DELETE /projects/:pID
// Route for deleting a single project
projectsRouter.delete("/:pID", function(req, res, next){
	req.project.remove(function(err){
		if(err) return next(err);
		Project.find(function(err, projects) {
			if(err) return next(err);
			res.json(projects);
		});
	});
});

/*=====  End of Project object routes  ======*/

/*==========================================
=            Task object routes            =
==========================================*/

// POST /projects/:pID/tasks
// Route for creating a task for a project
projectsRouter.post("/:pID/tasks", function(req, res, next){
	req.project.tasks.push(req.body);
	req.project.save(function(err, project) {
		if(err) return next(err);
		Project
		.findOne({_id: project._id})
		.populate('tasks.assignee')
		.exec(function(err, populatedProject) {
			if(err) return next(err);
			res.status(201);
			res.json(populatedProject);
		});
	});
});

// GET /projects/:pID/tasks/:taID
// Route for viewing a task for a project
projectsRouter.get("/:pID/tasks/:taID", function(req, res, next){
	res.json(req.task); // all the heavy lifting for this is done above in the params ID detection section
});

// PUT /projects/:pID/tasks/:taID
// Route for updating a task for a project
projectsRouter.put("/:pID/tasks/:taID", function(req, res, next){
	req.task.update(req.body, function(err, project) {
		if(err) return next(err);
		Project
		.findOne({_id: project._id})
		.populate('tasks.assignee')
		.exec(function(err, populatedProject) {
			if(err) return next(err);
			res.status(200);
			res.json(populatedProject);
		});
	});
});

// DELETE /projects/:pID/tasks/:taID
// Route for deleting a task for a project
projectsRouter.delete("/:pID/tasks/:taID", function(req, res, next){
	req.task.remove(function(err) {
		req.project.save(function(err, project) {
			if(err) return next(err);
			res.json(project);
		});
	});
});

// POST /projects/:pID/tasks/:taID/status
// Route for changing the status of a task for a project (complete / incomplete)
projectsRouter.post("/:pID/tasks/:taID/:status",
	function(req, res, next){
		if(req.params.status === "complete" || req.params.status === "incomplete") {
			req.status = req.params.status;
			next();
		} else {
			var err = new Error("Not Found");
			err.status = 404;
			next(err);
		}
	},
	function(req, res, next){
		req.task.status(req.status, function(err, project) {
			if(err) return next(err);
			res.json(project);
		});
});

/*=====  End of Task object routes  ======*/

module.exports = projectsRouter;