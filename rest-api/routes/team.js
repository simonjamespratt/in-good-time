'use strict';

var express = require('express');
var teamRouter = express.Router();
var TeamMember = require('../models/team_model.js');

/*==========================================
=            Param ID detection            =
==========================================*/

// This looks for a specified parameter (e.g. "tmID") in a request url.
// If found, it returns the document pertaining to that ID within the specified collection.
teamRouter.param("tmID", function(req,res,next,id){
	TeamMember.findById(id, function(err, doc){
		if(err) return next(err);
		if(!doc) {
			err = new Error("Not Found");
			err.status = 404;
			return next(err);
		}
		req.teamMember = doc;
		return next();
	});
});

/*=====  End of Param ID detection  ======*/

/*=================================================
=            Team member object routes            =
=================================================*/

// GET /team
// Route for team collection
teamRouter.get("/", function(req, res, next){
	TeamMember.find(function(err, members) {
		if(err) return next(err);
		res.json(members);
	});
});

// POST /team
// Route for creating a team member
teamRouter.post("/", function(req, res, next){
	var teamMember = new TeamMember(req.body);
	teamMember.save(function(err, teamMember) {
		if(err) return next(err);
		res.status(201);
		res.json(teamMember);
	});
});

// PUT /team/:tID
// Edit a team member
teamRouter.put("/:tmID", function(req, res){
	TeamMember.findByIdAndUpdate(req.params.tmID, {$set: req.body}, {new: true}, function(err, updatedTeamMember) {
		if(err) return next(err);
		res.json(updatedTeamMember);
	});
});


// DELETE /team/:tID
// Delete a team member
teamRouter.delete("/:tmID", function(req, res){
	req.teamMember.remove(function(err) {
		if(err) return next(err);
		TeamMember.find(function(err, members) {
			if(err) return next(err);
			res.json(members);
		});
	});
});

/*=====  End of Team member object routes  ======*/

module.exports = teamRouter;