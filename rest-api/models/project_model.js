'use strict';

var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var TaskSchema = new Schema({
	name: {
		type: String,
		trim: true,
	},
	start_date: Date,
	end_date: Date,
	description: {
		type: String,
		trim: true
	},
	assignee: {
		type: Schema.Types.ObjectId,
		ref: 'TeamMember'
	},
	complete: {
		type: Boolean,
		default: 0
	}
});

// Instance method for updating tasks within projects as the standard update method is not available on embedded documents 
TaskSchema.method("update", function(updates, callback) {
	Object.assign(this, updates);
	this.parent().save(callback);
});

// Instance method for updating the complete status on a task
TaskSchema.method("status", function(status, callback) {
	if (status === "complete") {
		this.complete = 1;
	} else {
		this.complete = 0;
	}
	this.parent().save(callback);
});


var ProjectSchema = new Schema({
	name: {
		type: String,
		trim: true,
	},
	start_date: Date,
	end_date: Date,
	tasks: [TaskSchema]
});

var Project = mongoose.model("Project", ProjectSchema);
module.exports = Project;
