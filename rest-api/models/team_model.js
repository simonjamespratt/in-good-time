'use strict';

var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var TeamMemberSchema = new Schema({
	name: {
		type: String,
		trim: true,
		unique: true, //this means that separate documents cannot have the same value for this field
		required: true
	}
});

var TeamMember = mongoose.model("TeamMember", TeamMemberSchema);
module.exports = TeamMember;
